import time

class PlaneList:
    def __init__(self):
        self.flightIDList = []
        self.flightList = []
        self.idLoc = 0

    def collect_flightID(self, data):
        if Plane.check_valid_data(data):
            self.flightIDList.append(data['flight'])
            self.flightList.append(Plane(data))

    def update_flights(self, data):
        self.return_flight_location(data)

        self.flightList[self.idLoc].update(data)


        # Better handling removal of planes 
        # If a plane object meets the timeout threashold and is deleted.
        # If it reappears and is recollected it will create a new object.
        #   -- Implement to timeouts a visually displayed timeout and an absolute timeout for deletion.
    def timeout(self, age=60):  # Age in seconds
        for ind, obj in enumerate(self.flightList):
            if time.time() - obj.time_update[-1] >= age:

                del (self.flightIDList[self.flightIDList.index(self.flightList[ind].flightID)])
                del (self.flightList[ind])
                # print(ind, time.time() - obj.time_update[-1])

    def clean_up(self):
        pass

    def return_flight_location(self, data, id=0):
        for index, idl in enumerate(self.flightList):

            if idl.flightID == data['flight']:
                self.idLoc = index


class Plane:
    def __init__(self, data, id=0):
        self.flightID = str(data['flight'])
        # self.squawk = data['squawk']
        self.latitude = [float(data['lat'])]
        self.longitude = [float(data['lon'])]
        self.altitude = [int(data['altitude'])]
        self.hex = [str(data['hex'])]
        self.speed = [int(data['speed'])]
        self.track = [int(data['track'])]
        self.time_update = [time.time()]
        self.index_location = id
        self.count = 0

    def update(self, data):
        # print len(self.location_history_lat)
        if float(data['lat']) != self.latitude[-1] and float(data['lon']) != self.longitude[-1] \
                and int(data['altitude']) != self.altitude[-1] and self.check_valid_data(data):

            self.count += 1
            self.flightID = str(data['flight'])
            self.latitude.append(float(data['lat']))
            self.longitude.append(float(data['lon']))
            self.altitude.append(int(data['altitude']))
            self.hex.append(str(data['hex']))
            self.track.append(int(data['track']))
            self.speed.append(int(data['speed']))
            self.time_update.append(time.time())

    @staticmethod
    def check_valid_data(data):
        if data['flight'] not in ['TEST1234', '       ', '????????','']:
            return True
        else:
            return False
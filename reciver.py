import requests as req, sqlite3 as sqb, os, time, json
from time import sleep as sleep, time
import database
from CoordinateMath import coordinateMath
from FlightClasses import *
import Display
url = 'http://localhost:8080/data.json'

db = database.DatabaseConnector('./basestation.sqb')
home_coordinates = {'latitude': 41.731280, 'longitude': -71.408050}



"""
ADD:
Curses module for live statistics.
////Except situation handling for Network comunications.
////Remodel the plane handling to incorporate Class structing
////Create functionality to handle/resolve active planes.
    -Additional functionality to display Active planes.
    -Additional functionality to remove Inactive planes.
    -Additional functionality to write to file every(seconds) within a given parameter.
        self.latitude = [float(data['lat'])]`
        self.longitude = [float(data['lon'])]
        self.altitude = [int(data['altitude'])]
        self.hex = [str(data['hex'])]
        self.speed = [int(data['speed'])]
        self.track = [int(data['track'])]

"""
label = ['Flight', ' Latitude', ' Longitude', ' Altitude', ' Hex   ', ' Speed', ' Track',' Last_Seen']
screen = Display.Display()
screen.draw_text_row(0, data=label)
screen.draw_row(1)
screen.draw_columns(1, data=label)

PlaneL = PlaneList()
count = 0
####
# Filtering
# Database broker
# Summary
while True:
    try:
        json_data = []
        try:
            json_data = req.get(url).json()

        except req.exceptions.RequestException as e:
            print (e)
            print ('Retrying in 5 seconds.')
            sleep(5)
        if json_data:
            for plane in json_data:
                if plane['flight'] not in PlaneL.flightIDList:

                    PlaneL.collect_flightID(plane)
                else:

                    PlaneL.update_flights(plane)

        sleep(0.5)
        screen.clear()
        for lineN, obj in enumerate(PlaneL.flightList):
            line = [obj.flightID, str(obj.latitude[-1]), str(obj.longitude[-1]), str(obj.altitude[-1]),
                    str(obj.hex[-1]), str(obj.speed[-1]), str(obj.track[-1]), str(round(time.time() - obj.time_update[-1]))]
            screen.draw_data_row(lineN+2, data=line, clone=label)
        screen.draw_text_row(0, data=label)
        screen.draw_row(1)
        screen.draw_columns(1, data=label)
        if count >= 20:
            PlaneL.timeout(30)
            count = 0
        count += 1
    except (KeyboardInterrupt, SystemExit):
        screen.end()
        exit(4)

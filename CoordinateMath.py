# coding=utf-8
import math


class coordinateMath:
    def __init__(self, locA, locB):
        self.coordinates_1 = {'lat': locA['latitude'], 'lon': locA['longitude']}
        self.coordinates_2 = {'lat': locB['latitude'], 'lon': locB['longitude']}
        self.R = 6371000
        self.phi_1 = math.radians(self.coordinates_1['lat'])
        self.phi_2 = math.radians(self.coordinates_2['lat'])
        self.tri_Phi = math.radians(self.coordinates_2['lat'] - self.coordinates_1['lat'])
        self.tri_Lam = math.radians(self.coordinates_2['lon'] - self.coordinates_1['lon'])

    def get_distance(self, output='km'):
        a = (math.sin(self.tri_Phi / 2) * math.sin(self.tri_Phi / 2)) + \
            math.cos(self.phi_1) * math.cos(self.phi_2) * \
            math.sin(self.tri_Lam / 2) * math.sin(self.tri_Lam / 2)

        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

        d = self.R * c

        if output == 'km':
            return d/1000.00
        else:
            return d/1609.34

    def get_bearing(self):
        pass


"""
JavaScript:	
var R = 6371e3; // metres
var φ1 = lat1.toRadians();
var φ2 = lat2.toRadians();
var Δφ = (lat2-lat1).toRadians();
var Δλ = (lon2-lon1).toRadians();

var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
        Math.cos(φ1) * Math.cos(φ2) *
        Math.sin(Δλ/2) * Math.sin(Δλ/2);
var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

var d = R * c;

"""

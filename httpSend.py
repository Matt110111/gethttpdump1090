import SocketServer
import json

class MyTCPServer(SocketServer.ThreadingTCPServer):
    allow_reuse_address = True

class MyTCPServerHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        try:
            data = self.request.recv(1024)
            #data = json.loads(self.request.recv(1024).strip())
            # process the data, i.e. print it:
            print data
            # send some 'ok' back
            self.request.sendall(json.dumps({'rd':''}))
        except Exception, e:
            print "Exception wile receiving message: ", e

server = MyTCPServer(('192.168.1.115', 8080), MyTCPServerHandler)

server.serve_forever()

import sqlite3 as sqb


class DatabaseConnector:
    def __init__(self, connection):
        self.connector = sqb.connect(connection)
        self.cursor = self.connector.cursor()

    def update_database(self, data):
        for obj in data:
                self.cursor.execute("INSERT INTO flightslog(flight,alt,heading,speed,lat,lon,last_update) VALUES(?,?,?,?,?,?,?)",
                    (obj.flightID, obj.altitude, obj.track, obj.speed, obj.latitude, obj.longitude, obj.time_updata))
                self.connector.commit()
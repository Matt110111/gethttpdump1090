# coding=utf-8
import time
import curses


class Display:

    def __init__(self):

        self.dis = curses.initscr()

    def draw_text_row(self, row, spacing=0, fill=8, data=[]):  # data is a list
        self.dis.addstr(row,0,' '.join(data))
        self.dis.refresh()

    def draw_data_row(self, row, data=[],clone=[]):  # data is a list
        new_data = []
        for integer, obj in enumerate(clone):
            temp = len(obj)
            string = data[integer] + ((temp - len(data[integer])) * ' ')
            new_data.append(string)
        self.dis.addstr(row,0,' '.join(new_data))
        self.dis.refresh()

    def draw_text_column(self, row, column, data):  # data is a list
        for x in range(len(data)):
            self.dis.addstr(x+row, column, data[x])
        self.dis.refresh()

    def draw_row(self, row, height=1,symbol='-'):
        total_columns = int(round(self.dis.getmaxyx()[1]/1.5))+4
        string = symbol * total_columns * height
        self.dis.addstr(row, 0, string)
        self.dis.refresh()

    def draw_column(self, column, width=1, symbol='|'):
        total_rows = self.dis.getmaxyx()[0]
        string = symbol * width
        for x in range(total_rows):
            self.dis.addstr(x, column, string)
        self.dis.refresh()

    def draw_columns(self, width=1, height=0, fill=7, data=[], symbol='|'):
        spacing = []
        kerning = 0
        for obj in data:
            kerning += len(obj)+1
            spacing.append(kerning)
        if height == 0:
            height = self.dis.getmaxyx()[0]-1
        for y in spacing:
            for x in range(height/2):
                self.dis.addstr(x, y, symbol)

        self.dis.refresh()

    def clear(self):
        pass

    @staticmethod
    def end():
        curses.endwin()







